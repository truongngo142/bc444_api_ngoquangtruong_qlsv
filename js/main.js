// get nfo and render form server
const BASE_URL = "https://643a58ccbd3623f1b9b16601.mockapi.io/sv";
function fetchSV() {
  batLoading();
  svService
    .getList()
    .then(function (res) {
      renderDSSV(res.data.reverse());
      tatLoading();
    })
    .catch(function (err) {
      console.log(err);
      tatLoading();
    });
}
fetchSV();
// add sv
function themSinhVien() {
  var sv = layThongTinTuForm();
  batLoading();
  svService
    .add(sv)
    .then(function (res) {
      fetchSV(res.data);
      tatLoading();
    })
    .catch(function (err) {
      console.log(err);
      tatLoading();
    });
}
// delete sv
function xoaSV(id) {
  batLoading();
  svService
    .delete(id)
    .then(function (res) {
      fetchSV(res.data);
      tatLoading();
      Toastify({
        text: "Deleted",
        duration: 3000,
        destination: "https://github.com/apvarun/toastify-js",
        newWindow: true,
        close: true,
        gravity: "top", // `top` or `bottom`
        position: "left", // `left`, `center` or `right`
        stopOnFocus: true, // Prevents dismissing of toast on hover
        style: {
          background: "linear-gradient(to right, #00b09b, #96c93d)",
        },
        onClick: function () {}, // Callback after click
      }).showToast();
    })
    .catch(function (err) {
      console.log(err);
      tatLoading();
    });
}
// fix

function suaSV(id) {
  batLoading();
  svService
    .fix(id)
    .then(function (res) {
      showThongTin(res.data);
      tatLoading();
    })
    .catch(function (err) {
      console.log(err);
      tatLoading();
    });
}
// update
function capNhatSinhVien() {
  batLoading();
  var sv = layThongTinTuForm();
  svService
    .update(sv)
    .then(function (res) {
      fetchSV();
      Toastify({
        text: "Updated",
        duration: 3000,
        destination: "https://github.com/apvarun/toastify-js",
        newWindow: true,
        close: true,
        gravity: "top", // `top` or `bottom`
        position: "left", // `left`, `center` or `right`
        stopOnFocus: true, // Prevents dismissing of toast on hover
        style: {
          background: "linear-gradient(to right, #00b09b, #96c93d)",
        },
        onClick: function () {}, // Callback after click
      }).showToast();
      tatLoading();
    })
    .catch(function (err) {
      console.log(err);
      tatLoading();
    });
}
