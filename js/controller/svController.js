// render dssv
function renderDSSV(dssv) {
  var contentHTML = "";
  for (i = 0; i < dssv.length; i++) {
    var sv = dssv[i];
    // sv là item của array DSSV
    var contentTr = `<tr>
    <td>${sv.ma}</td>
    <td>${sv.ten}</td>
    <td>${sv.mail}</td>
    <td>0</td>
    <td>
        <button class="btn btn-success" onclick="suaSV(${sv.ma})">Sửa</button>
        <button class="btn btn-danger" onclick="xoaSV(${sv.ma})">Xoá</button>
    </td>
    </tr>`;
    contentHTML += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

// lấy thông tin
function layThongTinTuForm() {
  var ma = document.getElementById("txtMaSV").value;
  var ten = document.getElementById("txtTenSV").value;
  var mail = document.getElementById("txtEmail").value;
  var matKhau = document.getElementById("txtPass").value;
  var toan = Number(document.getElementById("txtDiemToan").value);
  var ly = Number(document.getElementById("txtDiemLy").value);
  var hoa = Number(document.getElementById("txtDiemHoa").value);

  // tạo object sinh viên
  // var sv = {
  //   ma: ma,
  //   ten: ten,
  //   mail: mail,
  //   pass: matKhau,
  //   toan: toan,
  //   ly: ly,
  //   hoa: hoa,
  //   dtb: function () {
  //     return (this.hoa + this.toan + this.ly) / 3;
  //   },
  // };
  var sv = new Sinhvien(ma, ten, mail, matKhau, toan, ly, hoa);
  return sv;
}

//show thong tin len form
function showThongTin(sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.mail;
  document.getElementById("txtPass").value = sv.pass;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemHoa").value = sv.hoa;
}

// bật loading
function batLoading() {
  document.getElementById("loading").style.display = "flex";
}
// tắt loading
function tatLoading() {
  document.getElementById("loading").style.display = "none";
}
