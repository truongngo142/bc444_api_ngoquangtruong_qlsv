var svService = {
  getList: function () {
    return axios({
      url: BASE_URL,
      method: "GET",
    });
  },
  delete: function (id) {
    return axios({
      url: `https://643a58ccbd3623f1b9b16601.mockapi.io/sv/${id}`,
      method: "DELETE",
    });
  },
  add: function (sv) {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: sv,
    });
  },
  fix: function (id) {
    return axios({
      url: `https://643a58ccbd3623f1b9b16601.mockapi.io/sv/${id}`,
      method: "GET",
    });
  },
  update: function (sv) {
    return axios({
      url: `https://643a58ccbd3623f1b9b16601.mockapi.io/sv/${sv.ma}`,
      method: "PUT",
      data: sv,
    });
  },
};
