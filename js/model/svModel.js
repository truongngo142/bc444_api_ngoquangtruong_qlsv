function Sinhvien(_ma, _ten, _mail, _matKhau, _toan, _ly, _hoa) {
  this.ma = _ma;
  this.ten = _ten;
  this.mail = _mail;
  this.matKhau = _matKhau;
  this.toan = _toan;
  this.ly = _ly;
  this.hoa = _hoa;
  this.tinhDTB = function () {
    return (this.toan + this.ly + this.hoa) / 3;
  };
}
